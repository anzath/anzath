import {useRouter} from 'next/router';

import styles from './sidebar.module.css';

export default function Sidebar() {
  return (
    <div className={styles.root}>
      <NavHeaderItem text="Text Tools"/>
      <NavItem href="/apps/string-utilities" text="String Utilities"/>
      <NavItem href="/apps/lorem-ipsum" text="Lorem Ipsum"/>
    </div>
  );
}

declare type INavHeaderItemProps = {
  text: string;
}

function NavHeaderItem(props: INavHeaderItemProps) {
  return (
    <div className={styles.navHeaderItem}>
      {props.text}
    </div>
  );
}

declare type INavItemProps = {
  href: string;
  text: string;
};

function NavItem(props: INavItemProps) {
  const router = useRouter();

  const onClick = () => {
    router.push(props.href).catch(console.error);
  };

  return (
    <div className="px-2" onClick={onClick}>
      <div className={styles.navItem}>
        {props.text}
      </div>
    </div>
  );
}
