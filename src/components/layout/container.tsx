import styles from './container.module.css';

export default function Container(props) {
  const {children, ...moreProps} = props;

  return (
    <div className={styles.root} {...moreProps}>
      {children}
    </div>
  );
}
