declare type LabelProps = {
  text: string;
};

export default function Label(props: LabelProps) {
  return (
    <div style={{marginBottom: '4px'}}>
      <label style={{
        color: 'var(--ds-text-lowEmphasis, var(--ds-text-lowEmphasis, #6B778C))',
        fontSize: '12px',
        fontWeight: '600',
        lineHeight: '16px',
      }}>
        {props.text}
      </label>
    </div>
  );
}
