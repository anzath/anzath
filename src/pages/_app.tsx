import '@atlaskit/css-reset/dist/bundle.css';
import 'src/styles/globals.css';

import {Fragment} from 'react';
import Sidebar from 'src/components/layout/sidebar';

export default function MyApp({Component, pageProps}) {
  return (
    <Fragment>
      <Sidebar/>
      <Component {...pageProps} />
    </Fragment>
  );
}
