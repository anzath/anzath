import Head from 'next/head';
import {Fragment} from 'react';
import Container from 'src/components/layout/container';

export default function Page() {
  return (
    <Fragment>
      <Head>
        <title>irving.dev</title>
      </Head>

      <Container>
        <p>Hello, world!</p>
      </Container>
    </Fragment>
  );
}
