import Breadcrumbs, {BreadcrumbsItem} from '@atlaskit/breadcrumbs';
import Button from '@atlaskit/button';
import {Field} from '@atlaskit/form';
import PageHeader from '@atlaskit/page-header';
import Select from '@atlaskit/select';
import TextArea from '@atlaskit/textarea';
import TextField from '@atlaskit/textfield';
import {LoremIpsum} from 'lorem-ipsum';
import Head from 'next/head';
import {useEffect, useState} from 'react';
import Container from 'src/components/layout/container';

const TYPE_PARAGRAPHS = {label: 'Paragraphs', value: 1};
const TYPE_SENTENCES = {label: 'Sentences', value: 2};
const TYPE_WORDS = {label: 'Words', value: 3};

export default function Page() {
  const [inputType, setInputType] = useState(TYPE_PARAGRAPHS);
  const [inputNumber, setInputNumber] = useState(5);
  const [output, setOutput] = useState('');

  return (
    <Container>
      <Header/>

      <div className="flex flex-row">
        <div className="basis-1/3">
          <InputType inputType={inputType} setInputType={setInputType}/>
        </div>
      </div>

      <div className="flex flex-row mt-2">
        <div className="basis-1/3">
          <InputNumber inputType={inputType} inputNumber={inputNumber} setInputNumber={setInputNumber}/>
        </div>
      </div>

      <div className="mt-6">
        <OutputAction inputType={inputType} inputNumber={inputNumber} setOutput={setOutput}/>
      </div>

      <div className="mt-6">
        <OutputTextArea output={output}/>
      </div>
    </Container>
  );
}

function Header() {
  const breadcrumbs = (
    <Breadcrumbs>
      <BreadcrumbsItem href="#" text="Text Tools"/>
      <BreadcrumbsItem href="#" text="Lorem Ipsum"/>
    </Breadcrumbs>
  );

  return (
    <PageHeader breadcrumbs={breadcrumbs}>
      <Head>
        <title>Lorem Ipsum - irving.dev</title>
      </Head>

      Lorem Ipsum
    </PageHeader>
  );
}

function InputType({inputType, setInputType}: { inputType: any, setInputType: any }) {
  const options = [
    TYPE_PARAGRAPHS,
    TYPE_SENTENCES,
    TYPE_WORDS,
  ];

  const onChange = (e) => {
    setInputType(e);
  };

  return (
    <Field label="Type" id="input-type" name="input-type">
      {({fieldProps}: any) => {
        fieldProps.value = inputType;
        fieldProps.onChange = onChange;

        return <Select inputId={fieldProps.id} options={options} {...fieldProps}/>;
      }}
    </Field>
  );
}

function InputNumber(
  {
    inputType,
    inputNumber,
    setInputNumber
  }: {
    inputType: any,
    inputNumber: number,
    setInputNumber: any,
  }
) {
  const label = 'Number of ' + inputType.label;
  const onChange = (e) => {
    let v = parseInt(e.target.value);
    v = v >= 1 ? v : 1;
    v = v <= 100 ? v : 100;

    setInputNumber(v);
  };

  return (
    <Field label={label} id="input-number" name="input-number">
      {({fieldProps}: any) => {
        fieldProps.value = inputNumber;
        fieldProps.onChange = onChange;
        return <TextField type="number" {...fieldProps}/>;
      }}
    </Field>
  );
}

function OutputAction(
  {
    inputType,
    inputNumber,
    setOutput
  }: {
    inputType: any,
    inputNumber: number,
    setOutput: any,
  }
) {
  const onClick = () => {
    const loremIpsum = new LoremIpsum();

    let output = '';

    switch (inputType) {
      case TYPE_PARAGRAPHS:
        output = loremIpsum.generateParagraphs(inputNumber);
        break;
      case TYPE_SENTENCES:
        output = loremIpsum.generateSentences(inputNumber);
        break;
      case TYPE_WORDS:
        output = loremIpsum.generateWords(inputNumber);
        break;
    }

    setOutput(output);
  };

  return (
    <Button appearance="primary" onClick={onClick}>
      Generate
    </Button>
  );
}

function OutputTextArea({output}: { output: string }) {
  const [resize, setResize] = useState('auto');

  useEffect(() => {
    setResize('auto');
    setTimeout(() => {
      setResize('smart');
    }, 0)
  }, [output]);

  const styles = {
    lineHeight: '1.6',
  };

  return (
    <Field label="Generated Result" id="output-textarea" name="output-textarea">
      {({fieldProps}: any) => {
        fieldProps.value = output;
        return <TextArea resize={resize} minimumRows={8} isReadOnly {...fieldProps} style={styles}/>;
      }}
    </Field>
  );
}
