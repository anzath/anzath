import Breadcrumbs, {BreadcrumbsItem} from '@atlaskit/breadcrumbs';
import Button, {ButtonGroup} from '@atlaskit/button';
import {Field} from '@atlaskit/form';
import PageHeader from '@atlaskit/page-header';
import TextArea from '@atlaskit/textarea';
import TextField from '@atlaskit/textfield';
import Head from 'next/head';
import {useState} from 'react';
import Label from 'src/components/form/label';
import Container from 'src/components/layout/container';

export default function Page() {
  const [input, setInput] = useState('');
  const [output, setOutput] = useState('');

  return (
    <Container>
      <Header/>

      <div>
        <InputField input={input} setInput={setInput}/>
      </div>

      <div className="flex flex-row gap-4 mt-2">
        <div className="basis-1/2">
          <OutputCharacterCount input={input}/>
        </div>

        <div className="basis-1/2">
          <OutputWordCount input={input}/>
        </div>
      </div>

      <div className="mt-4">
        <OutputActions input={input} setOutput={setOutput}/>
      </div>

      <div className="mt-8">
        <OutputField output={output}/>
      </div>
    </Container>
  );
}

function Header() {
  const breadcrumbs = (
    <Breadcrumbs>
      <BreadcrumbsItem href="#" text="Text Tools"/>
      <BreadcrumbsItem href="#" text="String Utilities"/>
    </Breadcrumbs>
  );

  return (
    <PageHeader breadcrumbs={breadcrumbs}>
      <Head>
        <title>String Utilities - irving.dev</title>
      </Head>

      String Utilities
    </PageHeader>
  );
}

function InputField({input, setInput}: { input: string; setInput: any }) {
  const onChange = (e: any) => {
    setInput(e.target.value);
  }

  return (
    <Field label="Input String" id="input-string" name="input-string">
      {({fieldProps}: any) => {
        fieldProps.value = input;
        fieldProps.onChange = onChange;
        return <TextArea resize="none" minimumRows="8" {...fieldProps}/>;
      }}
    </Field>
  );
}

function OutputCharacterCount({input}: { input: string }) {
  return (
    <Field label="Character Count" id="output-character-count" name="output-character-count">
      {({fieldProps}: any) => {
        fieldProps.value = input ? input.trim().length : 0;
        return <TextField isReadOnly {...fieldProps}/>;
      }}
    </Field>
  );
}

function OutputWordCount({input}: { input: string }) {
  return (
    <Field label="Word Count" id="output-word-count" name="output-word-count">
      {({fieldProps}: any) => {
        fieldProps.value = input ? input.trim().split(/\s+/).length : 0;
        return <TextField isReadOnly {...fieldProps}/>;
      }}
    </Field>
  );
}

function OutputActions({input, setOutput}: { input: string, setOutput: any }) {
  const onClickLowercase = () => {
    setOutput(input.toLowerCase());
  };

  const onClickUppercase = () => {
    setOutput(input.toUpperCase());
  }

  const onClickReverse = () => {
    setOutput(input.split('').reverse().join(''));
  }

  return (
    <div>
      <Label text='Actions'/>

      <div>
        <ButtonGroup>
          <Button onClick={onClickLowercase}>Lowercase</Button>
          <Button onClick={onClickUppercase}>Uppercase</Button>
          <Button onClick={onClickReverse}>Reverse</Button>
        </ButtonGroup>
      </div>
    </div>
  );
}

function OutputField({output}: { output: string }) {
  return (
    <Field label="Output String" id="output-string" name="output-string">
      {({fieldProps}: any) => {
        fieldProps.value = output;
        return <TextArea resize="none" minimumRows="8" isReadOnly {...fieldProps}/>;
      }}
    </Field>
  );
}
